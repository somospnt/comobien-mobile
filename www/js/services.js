angular.module('starter.services', [])

/**
 * A simple example service that returns some data.
 */
.factory('Menus', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var menus = [{"id":1,"nombre":"Ensalada de la huerta","descripcion":"Un colchón de hortalizas con un mix de zanahoria, tomate, remolacha, chauchas.&#10;Solamente 200 kcal. la porción.<p><br/></p><h3>Ingredientes</h3><ol><li>Azucar</li><li>Harina</li></ol>","imagen":"vianda_1.jpg","categoria":{"id":5,"nombre":"Vegetarianas"},"visible":true},{"id":3,"nombre":"Medallones de lomito","descripcion":"Medallones de lomito con salteado de verduras.\r\n400 kcal. la porción","imagen":"vianda_4.jpg","categoria":{"id":1,"nombre":"Carnes"},"visible":false},{"id":4,"nombre":"Spaguetti al brócoli","descripcion":"Fideos con una salsa liviana a base de brócoli.\r\n400 kcal. la porción.","imagen":"vianda_5.jpg","categoria":{"id":4,"nombre":"Pastas"},"visible":true},{"id":30,"nombre":"Menu para demo","descripcion":"con imagen de 2 mb","imagen":"menu.JPG","categoria":{"id":7,"nombre":"Meriendas"},"visible":true},{"id":39,"nombre":"Chop suey de pollo y camarones","descripcion":"Chop suey de pollo y camarones acompañados pintado con una fina salsa de maracuyá. (Preguntar por disponibilidad).\r\n300 kcal. la porción","imagen":"vianda_1.jpg","categoria":{"id":2,"nombre":"Pollos"},"visible":true}];

  return {
    all: function() {
      return menus;
    },
    get: function(menusId) {
      // Simple index lookup
      return menus[menuId];
    }
  }
});